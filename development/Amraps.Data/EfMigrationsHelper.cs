﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Amraps.Data.Entities;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Amraps.Data
{
    public class TemporaryAmrapsDbContext : IDesignTimeDbContextFactory<AmrapsDbContext>
    {
        public AmrapsDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<AmrapsDbContext>();
            var migration = typeof(AmrapsDbContext).GetTypeInfo().Assembly.GetName().Name;
            builder.UseSqlServer("Server=(local);Database=DevUp2017;Trusted_Connection=True;MultipleActiveResultSets=true",
                b => b.MigrationsAssembly(migration));
            return new AmrapsDbContext(builder.Options);
        }
    }

    public class TemporaryPersistedGrantDbContext : IDesignTimeDbContextFactory<PersistedGrantDbContext>
    {
        public PersistedGrantDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PersistedGrantDbContext>();
            var migration = typeof(AmrapsDbContext).GetTypeInfo().Assembly.GetName().Name;
            builder.UseSqlServer("Server=(local);Database=DevUp2017;Trusted_Connection=True;MultipleActiveResultSets=true",
                b => b.MigrationsAssembly(migration));
            var opOptions = new OperationalStoreOptions { DefaultSchema = "auth" };
            return new PersistedGrantDbContext(builder.Options, opOptions);
        }
    }

    public class TemporaryConfigurationDbContext : IDesignTimeDbContextFactory<ConfigurationDbContext>
    {
        public ConfigurationDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ConfigurationDbContext>();
            var migration = typeof(AmrapsDbContext).GetTypeInfo().Assembly.GetName().Name;
            builder.UseSqlServer("Server=(local);Database=DevUp2017;Trusted_Connection=True;MultipleActiveResultSets=true",
                b => b.MigrationsAssembly(migration));
            var storeOptions = new ConfigurationStoreOptions { DefaultSchema = "auth" };
            return new ConfigurationDbContext(builder.Options, storeOptions);
        }
    }
}
