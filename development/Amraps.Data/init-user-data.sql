USE [amraps]
GO
SET IDENTITY_INSERT [dbo].[Affiliates] ON 

GO
INSERT [dbo].[Affiliates] ([AffiliateId], [CreatedDate], [Name], [EmailAddress], [PhoneNumber], [Website], 
   [PlaceId], [StreetAddress], [StreetAddress2], [City], [State], [PostalCode], [CreatedByUser]) 
VALUES (1, CAST(N'2017-10-11 00:00:00.0000000' AS DateTime2), N'BARx Crossfit LLC', N'info@barxcrossfit.local', N'3145551212', N'https://www.barxcrossfit.com', 
    N'ChIJTx3V1AnO2IcRHgLjBLBl250', N'12309 Old Big Bend Road', NULL, N'Kirkwood', N'MO', N'63122',  N'Master Overseer')
GO
INSERT [dbo].[Affiliates] ([AffiliateId], [CreatedDate], [Name], [EmailAddress], [PhoneNumber], [Website], 
     [PlaceId], [StreetAddress], [StreetAddress2], [City], [State], [PostalCode], [CreatedByUser]) 
VALUES (3, CAST(N'2017-10-11 00:00:00.0000000' AS DateTime2), N'Crossfit 314', N'dl@cf413.local', N'3145551212', N'https://crossfit-314.local', 
     N'ChIJidNTbfjI2IcRpcMrqelxEJE', N'11133 Lindbergh Business Ct', NULL, N'St. Louis', N'MO', N'63123', N'Master Overseer')
GO
INSERT [dbo].[Affiliates] ([AffiliateId], [CreatedDate], [Name], [EmailAddress], [PhoneNumber], [Website], 
     [PlaceId], [StreetAddress], [StreetAddress2], [City], [State], [PostalCode], [CreatedByUser]) 
VALUES (4, CAST(N'2017-10-11 00:00:00.0000000' AS DateTime2), N'Mile High Crossfit', N'mh-info@mhxfit.local', N'4335551212', N'https://mh-xfit.local', 
    N'ChIJFQ1Jd0iPbIcRpVRdPF0AdKU', N'14185 E Easter Ave', NULL, N'Centennial', N'CO', N'80112', N'Master Overseer')
GO
SET IDENTITY_INSERT [dbo].[Affiliates] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

GO
INSERT [dbo].[Users] ([UserId], [AccessFailedCount], [ConcurrencyStamp], [DefaultLocationId], [Email], [EmailConfirmed], [FirstName], [IsDeleted], [LastName], [LockoutEnabled], [LockoutEnd], [NormalizedEmail], [NormalizedUserName], [PasswordHash], [PhoneNumber], [PhoneNumberConfirmed], [SecurityStamp], [TwoFactorEnabled], [UserName]) VALUES (1, 0, N'9e3fc90b-9a70-4178-a43b-d3ae05379018', NULL, N'master_overseer@amraps.local', 1, N'Master', 1, N'Overseer', 1, NULL, N'MASTER_OVERSEER@AMRAPS.LOCAL', N'MASTER_OVERSEER', N'AQAAAAEAACcQAAAAEE9T7Th+W50ABeTaBn726dhcaPS9T+tMx7YQ0EZ54IOayR9sea6d9lN/B4B3l7/6FA==', NULL, 0, N'983d0920-8d30-45ec-be5f-494f1cefc56c', 0, N'master_overseer')
GO
INSERT [dbo].[Users] ([UserId], [AccessFailedCount], [ConcurrencyStamp], [DefaultLocationId], [Email], [EmailConfirmed], [FirstName], [IsDeleted], [LastName], [LockoutEnabled], [LockoutEnd], [NormalizedEmail], [NormalizedUserName], [PasswordHash], [PhoneNumber], [PhoneNumberConfirmed], [SecurityStamp], [TwoFactorEnabled], [UserName]) VALUES (2, 0, N'f14f0bc8-1ac9-4ced-8848-c3a21828a954', NULL, N'jeff@amraps.local', 1, N'Jeff', 1, N'St. Germain', 1, NULL, N'JEFF@AMRAPS.LOCAL', N'JEFF_AMRAPS', N'AQAAAAEAACcQAAAAEE9T7Th+W50ABeTaBn726dhcaPS9T+tMx7YQ0EZ54IOayR9sea6d9lN/B4B3l7/6FA==', NULL, 0, N'983d0920-8d30-45ec-be5f-494f1cefc56c', 0, N'jeff_amraps')
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO

SET IDENTITY_INSERT [dbo].[Roles] ON 

GO
INSERT [dbo].[Roles] ([RoleId], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (3, N'e4fb915c-feaa-4012-8e9f-95788e043166', N'affiliates', N'AFFILIATES')
GO
INSERT [dbo].[Roles] ([RoleId], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (4, N'd299e2cf-21e4-4c3a-8594-96a9f917a195', N'affiliates.readonly', N'AFFILIATES.READONLY')
GO
INSERT [dbo].[Roles] ([RoleId], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (5, N'fb71da55-ce14-4a53-92cc-04c138e5d078', N'locations', N'LOCATIONS')
GO
INSERT [dbo].[Roles] ([RoleId], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (6, N'617d8ce9-c376-40d7-bd47-e646b1dea5b6', N'locations.readonly', N'LOCATIONS.READONLY')
GO
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO
INSERT [dbo].[UserRoles] ([RoleId], [UserId]) VALUES (3, 1)
GO
INSERT [dbo].[UserRoles] ([RoleId], [UserId]) VALUES (4, 1)
GO
INSERT [dbo].[UserRoles] ([RoleId], [UserId]) VALUES (5, 1)
GO
INSERT [dbo].[UserRoles] ([RoleId], [UserId]) VALUES (5, 2)
GO
INSERT [dbo].[UserRoles] ([RoleId], [UserId]) VALUES (6, 1)
GO
INSERT [dbo].[UserRoles] ([RoleId], [UserId]) VALUES (6, 2)
GO
