﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Amraps.Data.Entities
{
    public class AmrapsDbContext : IdentityDbContext<AppUser, AppRole, int>
    {
        public AmrapsDbContext(DbContextOptions<AmrapsDbContext> options) 
            : base(options) { }

        public DbSet<AffiliateEntity> Affiliates { get; set; }
        public DbSet<LocationEntity> Locations { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<AppUser>(e =>
            {
                e.ToTable("Users");
                e.Property(p => p.Id).HasColumnName("UserId");
            });

            builder.Entity<AppRole>()
                .ToTable("Roles")
                .Property(p => p.Id).HasColumnName("RoleId");

            builder.Entity<IdentityUserRole<int>>()
                .ToTable("UserRoles")
                .HasKey(p => new { p.RoleId, p.UserId });

            builder.Entity<IdentityUserClaim<int>>()
                .ToTable("UserClaims")
                .Property(e => e.Id)
                .HasColumnName("UserClaimId");

            builder.Entity<IdentityRoleClaim<int>>()
                .ToTable("RoleClaims")
                .Property(e => e.Id)
                .HasColumnName("RoleClaimId");

            builder.Entity<IdentityUserLogin<int>>()
                .ToTable("UserLogins")
                .HasKey(p => new {p.LoginProvider, p.ProviderKey});

            builder.Entity<IdentityUserToken<int>>()
                .ToTable("UserTokens")
                .HasKey(p => new { p.UserId, p.LoginProvider, p.Name });
        }
    }
}
