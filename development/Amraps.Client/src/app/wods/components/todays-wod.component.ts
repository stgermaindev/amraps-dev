import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Wod, WodsClient} from '../../services/api.clients';

@Component({
    selector: 'todays-wod',
    templateUrl: './todays-wod.component.html'
})
export class TodaysWodComponent implements OnInit {
    public wod: Wod;
    public error: boolean = false;
    public loading: boolean = false;

    constructor(private route: ActivatedRoute,
                private _wodsClient: WodsClient) {

    }

    public ngOnInit(): void {
      this.loading = true;
       this._wodsClient.getTodaysWod().subscribe((wod: Wod) => {
         this.wod = wod;
         this.loading = false;
         this.error = false;
       }, (err) => {
         this.error = true;
         this.loading = false;
       });
    }

    public get workout(): string {
        if (!this.wod) {
            return 'Error loading Wod';
        }
        if (!this.wod.sections) {
            return 'Error loading Wod';
        }
        let html = '';
        for (const s of this.wod.sections) {
            if (!s.components) {
                continue;
            }
            html += '<dl>'
            html += `<dt>${s.name}</dt>`
            for (const c of s.components) {
                html += `<dd class="ml-2">${c.name}</dd>`;
            }
            html += '</dl>';
        }

        return html;
    }

}
