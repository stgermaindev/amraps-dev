import {NgModule} from '@angular/core';
import {TodaysWodComponent} from './components/todays-wod.component';
import {CommonModule} from '@angular/common';
import {WodsClient} from '../services/api.clients';
import {RouterModule} from '@angular/router';
import {WodRoutes} from './wods.routes';

@NgModule({
    declarations: [TodaysWodComponent],
    imports: [
      CommonModule,
      RouterModule.forChild(WodRoutes)],
    exports: [],
    providers: [WodsClient]
})
export class WodsModule {

}
