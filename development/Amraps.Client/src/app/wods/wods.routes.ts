import {Routes} from '@angular/router';
import {TodaysWodComponent} from './components/todays-wod.component';


export const WodRoutes: Routes = [{
  path: '',
  children: [{
    path: 'todays-wod',
    component: TodaysWodComponent
  }]
}];
