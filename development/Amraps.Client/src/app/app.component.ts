import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthService} from './auth/services/auth.service';
import {NavigationStart, Router} from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  constructor(private _authService: AuthService, private router: Router) {

  }

  public ngOnInit(): void {
    this._authService.configureAuthentication();
    this.router.events.filter(evt => evt instanceof(NavigationStart))
      .subscribe((evt: NavigationStart) => {
        console.log(evt);
      });
  }
}
