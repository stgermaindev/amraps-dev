import {INavigationPaths} from './INavigationPaths';


export const NavigationPaths: INavigationPaths[] = [
  {
    path: '/app/wods/todays-wod',
    title: 'Today\'s Wod',
    iconType: 'today'
  },
  {
    path: '/app/wods/list',
    title: 'Wods',
    iconType: 'fitness_center'
  }, {
    path: '/athletes',
    title: 'Athletes',
    iconType: 'perm_identity',
    children: [{
      path: 'leads',
      title: 'Leads'
    }, {
      path: 'list',
      title: 'List'
    }]
  }, {
    path: '/classes',
    title: 'Classes',
    iconType: 'event',
    children: [{
      path: 'list',
      title: 'List'
    }, {
      path: 'locations',
      title: 'Locations',
    }]
  }, {
    path: '/app/affiliates/list',
    title: 'Affiliates',
    authorize: ['affiliates.readonly'],
    iconType: 'business'
  }, {
    path: '/financial',
    title: 'Financial',
    iconType: 'attach_money',
    children: [{
      path: 'list',
      title: 'List',
    }]
  }, {
    path: '/reports',
    title: 'Reports',
    iconType: 'pie_chart',
    children: [{
      path: 'list',
      title: 'List',
    }]
  }
];
