import {Routes} from '@angular/router';
import {ListAffiliatesComponent} from './list/list-affiliates.component';
import {EditAffiliateComponent} from './edit/edit-affiliate.component';
import {AdminLayoutComponent} from '../layouts/admin.layout.component';
import {AuthorizedGuard} from '../auth/guards/authorized.guard';

export const AffiliatesRouting: Routes = [{
    path: '',
    children: [{
        path: 'list',
        component: ListAffiliatesComponent,
    }, {
        path: 'edit/:id',
        component: EditAffiliateComponent,
        // data: {
        //    authorize: ['affiliates']
        // }
    }]
}];
