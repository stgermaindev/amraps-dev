import {NgModule} from '@angular/core';
import {ListAffiliatesComponent} from './list/list-affiliates.component';
import {CommonModule} from '@angular/common';
import {EditAffiliateComponent} from './edit/edit-affiliate.component';
import {AffiliatesClient} from '../services/api.clients';
import {RouterModule} from '@angular/router';
import {AffiliatesRouting} from './affiliates.routing';
import {AuthModule} from '../auth/auth.module';

@NgModule({
  declarations: [
    ListAffiliatesComponent,
    EditAffiliateComponent
  ],
  imports: [
    AuthModule,
    CommonModule,
    RouterModule.forChild(AffiliatesRouting)
  ],
  exports: [
    ListAffiliatesComponent,
    EditAffiliateComponent
  ],
  providers: [
    AffiliatesClient
  ]
})
export class AffiliatesModule {

}



