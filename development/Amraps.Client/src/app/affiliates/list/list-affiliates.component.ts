import {Component, OnInit} from '@angular/core';
import {Address, Affiliate, AffiliatesClient} from '../../services/api.clients';
import {Router} from '@angular/router';

@Component({
  selector: 'list-affiliates',
  templateUrl: './list-affiliates.component.html'
})
export class ListAffiliatesComponent implements OnInit {
  public affiliates: Affiliate[] = [];

  constructor(private _affiliatesClient: AffiliatesClient,
              private _router: Router) {

  }

  public ngOnInit(): void {
    this._affiliatesClient.getAll().subscribe((data: Affiliate[]) => {
      this.affiliates = data;
    });
  }

  public buildStreetAddress(address: Address | null): string {
    if (!address)
      return '';

    let addr = address.streetAddress;
    addr += address.streetAddress2 ? ' ' + address.streetAddress2 : '';

    return addr as string;
  }

  public onEditTerminal(aff: Affiliate) {
    this._router.navigate(['edit', aff.affiliateId])
  }

  public onDelete(aff: Affiliate) {
    this._affiliatesClient.delete(aff.affiliateId).subscribe(() => {
      this.affiliates = this.affiliates.filter(a => a.affiliateId != aff.affiliateId);
    });
  }
}
