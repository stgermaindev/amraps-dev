using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Amraps.Business.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Amraps.Data.Entities;

namespace Amraps.Business.Managers
{
    public interface IWodManager
    {
        Task<Wod> GetWod(int id);
        Task<Wod> GetTodaysWod();
    }

    public class WodManager : IWodManager
    {
        private readonly AmrapsDbContext _context;

        public WodManager(AmrapsDbContext context)
        {
            _context = context;
        }

        public async Task<Wod> GetWod(int id)
        {
            return await Task.FromResult(new Wod());
        }

        public async Task<Wod> GetTodaysWod()
        {
            var wod = BuildTodaysWod();
            return await Task.FromResult(wod);
        }

        public Task<List<Wod>> GetWods()
        {
            return null;
        }

        #region Private Helper Methods

        private static Wod BuildTodaysWod()
        {
            var wod = new Wod
            {
                Id = 1,
                Name = DateTime.Today.ToString("MM/dd/yyyy"),
                WodDate = DateTime.Today
            };
            var ssSection = new WodSection
            {
                Id = 1,
                Name = "Strength/Skill"
            };
            var metSection = new WodSection
            {
                Id = 2,
                Name = "Metcon (Time)"
            };

            ssSection.Components.Add(new WodComponent
            {
                Id = 1,
                Name = "A1): Push Press (Find 5RM for today)"
            });
            ssSection.Components.Add(new WodComponent
            {
                Id = 2,
                Name = "B1): Weighted strict Pullups: Increase weight each set"
            });
            ssSection.Components.Add(new WodComponent
            {
                Id = 3,
                Name = "B2): Barbell side bends (empty Barbell overhead)"
            });
            ssSection.Components.Add(new WodComponent
            {
                Id = 4,
                Name = "B3): 500m row"
            });
            metSection.Components.Add(new WodComponent
            {
                Id = 4,
                Name = @"1-2-3-4-5-6-7-8-9-10<br/>
                    Push Jerk 115lbs Men | 75lbs Women<br/>
                    Hang Power Cleans 115lbs Men | 75lbs Women <br/>
                    *8x10m Shuttle Runs between each set"
            });
            wod.Sections.Add(ssSection);
            wod.Sections.Add(metSection);
            return wod;
        }

        #endregion
    }
}


