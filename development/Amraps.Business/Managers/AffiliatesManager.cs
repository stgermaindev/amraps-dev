﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Amraps.Business.Models;
using Amraps.Data.Entities;
using System.Linq;
using Microsoft.EntityFrameworkCore;


namespace Amraps.Business.Managers
{
    public interface IAffiliatesManager
    {
        Task<IEnumerable<Affiliate>> AllAffiliates();
        Task<Affiliate> GetAffiliate(int id);
        Task<Affiliate> UpdateAffiliate(int id, Affiliate affiliate);
        Task<Affiliate> AddAffiliate(Affiliate affiliate);
        Task DeleteAffiliate(int id);
    }

    public class AffiliatesManager : IAffiliatesManager
    {
        private readonly AmrapsDbContext _context;
        
        public AffiliatesManager(AmrapsDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Affiliate>> AllAffiliates()
        {
            return await _context.Affiliates.Select(a => new Affiliate(a)).ToListAsync();
        }

        public async Task<Affiliate> GetAffiliate(int id)
        {
            var entity = await _context.Affiliates.FindAsync(id);
            return entity != null ? new Affiliate(entity) : null;
        }

        public Task<Affiliate> UpdateAffiliate(int id, Affiliate affiliate)
        {
            throw new NotImplementedException();
        }

        public Task<Affiliate> AddAffiliate(Affiliate affiliate)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAffiliate(int id)
        {
            return Task.Run(() => { return; });
        }
    }
}
