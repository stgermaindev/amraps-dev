﻿using System;
using System.Net;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;

namespace Amraps.Apis
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var envVar = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var isDev = !string.IsNullOrWhiteSpace(envVar) && envVar == EnvironmentName.Development;
            
            var host = WebHost.CreateDefaultBuilder(args)
                 .UseKestrel().UseUrls("http://localhost:4001")
                 .ConfigureAppConfiguration((context, config) =>
                 {
                     config.Sources.Clear();
                     config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                     config.AddJsonFile($"appsettings.{context.HostingEnvironment.EnvironmentName}.json", optional: true);
                     config.AddEnvironmentVariables();
                 })
                .ConfigureLogging((context, builder) =>
                {
                    var logger = new LoggerConfiguration()
                        .MinimumLevel.Debug()
                        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                        .Enrich.FromLogContext()
                        .WriteTo.Console()
                        .CreateLogger();
                    Log.Logger = logger;
                })
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
