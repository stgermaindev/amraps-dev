﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amraps.Apis.Models
{
    public class ErrorResponse
    {
        public ErrorResponse()
        {
            Errors = new List<Error>();
        }

        public string Message { get; set; }
        public int ResultCode { get; set; }
        public string ReferenceNumber { get; set; }
        public List<Error> Errors { get; set; }
    }
}
