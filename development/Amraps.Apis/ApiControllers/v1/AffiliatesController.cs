﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Amraps.Apis.Models;
using Amraps.Business.Managers;
using Amraps.Business.Models;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using Microsoft.AspNetCore.Authorization;

namespace Amraps.Apis.ApiControllers.v1
{
    [Route("api/v1/affiliates")]
    public class AffiliatesController : ApiControllerBase
    {
        private readonly IAffiliatesManager _affiliatesManager;

        public AffiliatesController(IAffiliatesManager affiliatesManager)
        {
            _affiliatesManager = affiliatesManager;
        }
        
        [HttpGet]
        [SwaggerResponse(typeof(List<Affiliate>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
        [Authorize(Roles = "affiliates.readonly")]
        public async Task<IActionResult> Get()
        {
            var model = await _affiliatesManager.AllAffiliates();
            return Ok(model);
        }
       
        [HttpGet("{id}")]
        [SwaggerResponse(typeof(Affiliate))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
        [Authorize(Roles = "affiliates.readonly")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            var model = await _affiliatesManager.GetAffiliate(id);
            if (model != null)
                return Ok(model);

            return ErrorResult(400, "Bad Request", null,
                new[] { new Error { Reason = "Bad Request", Message = "Bad Request" } });
        }
        
        [HttpPut("{id}")]
        [SwaggerResponse(typeof(Affiliate))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
        [Authorize(Roles = "affiliates")]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] Affiliate affiliate)
        {
            var model = await _affiliatesManager.UpdateAffiliate(id, affiliate);
            if (model != null)
                return Ok(model);

            return ErrorResult(400, "Bad Request", null,
                new[] { new Error { Reason = "Bad Request", Message = "Bad Request" } });
        }
        
        
        [HttpPost]
        [SwaggerResponse(typeof(Affiliate))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
        [Authorize(Roles = "affiliates")]
        public async Task<IActionResult> Post([FromBody] Affiliate affiliate)
        {
            var model = await _affiliatesManager.AddAffiliate(affiliate);
            if (model != null)
                return Ok(model);

            return ErrorResult(400, "Bad Request", null,
                new[] { new Error { Reason = "Bad Request", Message = "Bad Request" } });
        }
        
        [HttpDelete("{id}")]
        [SwaggerResponse(typeof(void))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
        [Authorize(Roles = "affiliates")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            await _affiliatesManager.DeleteAffiliate(id);
            return NoContent();
        }
    }
}

