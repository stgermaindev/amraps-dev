using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Amraps.Apis.Models;
using Amraps.Business.Managers;
using Amraps.Business.Models;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using Microsoft.AspNetCore.Authorization;

namespace Amraps.Apis.ApiControllers.v1
{
    [Authorize]
    [Route("api/v1/wods")]
    public class WodsController : ApiControllerBase
    {
        private readonly IWodManager _wodManager;
        public WodsController(IWodManager wodManager)
        {
            _wodManager = wodManager;
        }

        [HttpGet]
        [SwaggerResponse(typeof(Wod))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
        public async Task<IActionResult> GetList()
        {
            return Ok(await Task.FromResult(new Wod()));
        }
        
        
        [HttpGet("{id}")]
        [SwaggerResponse(typeof(Wod))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
        public async Task<IActionResult> Get([FromRoute]int id)
        {
            return Ok(await Task.FromResult(new Wod()));
        }

        [HttpGet("today")]
        [SwaggerResponse(typeof(Wod))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
        public async Task<IActionResult> GetTodaysWod()
        {
            var model = await _wodManager.GetTodaysWod();           
            if (model != null)
                return Ok(model);

            return ErrorResult(400, "Bad Request", null,
                new[] { new Error { Reason = "Bad Request", Message = "Bad Request" } });
        }

    }
}