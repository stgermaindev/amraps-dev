﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Amraps.Data.Entities;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Amraps.Identity.Stores
{
    public class AmrapsProfileService : IProfileService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly AmrapsDbContext _context;
        
        public AmrapsProfileService(UserManager<AppUser> userManager, AmrapsDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }
        
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var sUserId = _userManager.GetUserId(context.Subject);
            if(string.IsNullOrEmpty(sUserId))
                return;

            var uid = int.Parse(sUserId);
            var userTask = _context.Users.FindAsync(uid);
            var roleTask = _context.UserRoles.Where(u => u.UserId == uid).Select(u => u.RoleId).ToListAsync();
            Task.WaitAll(userTask, roleTask);

            var user = await userTask;
            var roleIds = await roleTask;
            var roleNames = await _context.Roles.Where(r => roleIds.Contains(r.Id)).Select(r => r.Name).ToListAsync();
            
            var claims = new List<Claim>
            {
                new Claim("name", user.UserName, ClaimTypes.Name),
                new Claim("given_name", $"{user.FirstName} {user.LastName}", ClaimTypes.GivenName),
                new Claim("email", user.Email, ClaimTypes.Email)
            };
            claims.AddRange(roleNames.Select(name => new Claim("role", name, ClaimTypes.Role)));
            context.IssuedClaims.AddRange(claims);
            
            return;
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            var sUserId = _userManager.GetUserId(context.Subject);
            if (string.IsNullOrEmpty(sUserId))
                context.IsActive = false;

            context.IsActive = true;
            return Task.FromResult(0); 
        }
    }
}
