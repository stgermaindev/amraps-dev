﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amraps.Identity.Configuration
{
    public class IdentityServerConfiguration
    {
        public string AuthorityUrl { get; set; }
        public BaseUrls BaseUrls { get; set; }
    }

    public class BaseUrls
    {
        public string Web { get; set; }
        public string Admin { get; set; }
    }
}
