﻿using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using Amraps.Data.DataSeed;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Amraps.Data.Entities;
using Amraps.Identity.Configuration;
using Amraps.Identity.Stores;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Amraps.Data.Extensions;
using IdentityServer4.Services;

namespace Amraps.Identity
{
    public class Startup
    {
        private readonly IHostingEnvironment _env;
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //Load dbConnection string;
            var amrapsConnection = Configuration.GetConnectionString("AmrapsConnection");
            //Get the name of migrations assembly.
            var migrationAssembly = typeof(AmrapsDbContext).GetTypeInfo().Assembly.GetName().Name;

            //Load identityserver config from file and set into configuration.
            services.AddOptions();
            services.Configure<IdentityServerConfiguration>(Configuration.GetSection("IdentityServerConfiguration"));

            //Add dbcontext. 
            services.AddDbContext<AmrapsDbContext>(options => options.UseSqlServer(amrapsConnection));

            //****** Configure Identity
            services.AddIdentity<AppUser, AppRole>(opt =>
            {
                opt.Password.RequireDigit = true;
                opt.Password.RequireUppercase = true;
                opt.Password.RequiredLength = 8;
                opt.Password.RequireNonAlphanumeric = true;
            }).AddEntityFrameworkStores<AmrapsDbContext>()
              .AddDefaultTokenProviders();
             
            services.AddTransient<IClientStore, AmrapsClientsStore>();
            services.AddTransient<IResourceStore, AmrapsResourceStore>();
            services.AddTransient<IProfileService, AmrapsProfileService>();

            //****** Configure Identity Server
            var identityBuilder = services.AddIdentityServer(opt =>
                {
                    opt.UserInteraction.LoginUrl = "/auth/login";
                    opt.UserInteraction.LogoutUrl = "/auth/logout";
                }).AddConfigurationStore(so =>
                {
                    so.ConfigureDbContext = (db) => db.UseSqlServer(amrapsConnection, options => options.MigrationsAssembly(migrationAssembly));
                    so.DefaultSchema = "auth";
                }).AddOperationalStore(so =>
                {
                    so.ConfigureDbContext = (db) => db.UseSqlServer(amrapsConnection, options => options.MigrationsAssembly(migrationAssembly));
                    so.DefaultSchema = "auth";
                })
                .AddAspNetIdentity<AppUser>()
                .AddClientStore<AmrapsClientsStore>()
                .AddProfileService<AmrapsProfileService>()
                .AddResourceStore<AmrapsResourceStore>();

            //Handle certificate for Identity Server
            if (_env.IsProduction())
                identityBuilder.AddSigningCredential(LoadCertificate());
            else
                identityBuilder.AddDeveloperSigningCredential();
            //***** End Identity Server Config.

            if (_env.IsDevelopment())
                services.AddSingleton<IDataSeeder, DevDataSeeder>();

            // Forces redirect to HTTPS
            // services.AddMvc(opt =>
            // {
            //     opt.Filters.Add(new RequireHttpsAttribute());
            //      opt.SslPort = 4000;
            // });
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();
            loggerFactory.AddDebug(); 

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage(); 
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
                app.UseCors(p => p.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            }
            else
            {
                app.UseExceptionHandler("/Home/Error"); 
            }

            //***** Tell the app to use Identity authentication
            app.UseAuthentication();
            //***** Tell the app to use Identity Server
            app.UseIdentityServer();

            app.UseStaticFiles();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.InitializeDatabase();
        }

        private static X509Certificate2 LoadCertificate()
        {
            //Look up cert in store by thumbprint.
            using (var certStore = new X509Store(StoreName.My, StoreLocation.CurrentUser))
            {
                certStore.Open(OpenFlags.ReadOnly);
                //TODO: Load thumbprint from config or key vault.
                var certCollection = certStore.Certificates.Find(X509FindType.FindByThumbprint, "<THUMBPRINT HERE>", false);
                return certCollection.Count > 0 ? certCollection[0] : null;
            }
        }
    }
}
