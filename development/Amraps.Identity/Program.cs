﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace Amraps.Identity
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var envVar = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var isDev = !string.IsNullOrWhiteSpace(envVar) && envVar == EnvironmentName.Development;

            /*
            .UseKestrel(opt =>
                {
                    if (isDev)
                        opt.Listen(IPAddress.Loopback, 4000, l => { l.UseHttps("amraps.local.pfx", "pass@word1"); });
                })
            */

            var host = WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                 .ConfigureAppConfiguration((context, config) =>
                 {
                     config.Sources.Clear();
                     config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                     config.AddJsonFile($"appsettings.{context.HostingEnvironment.EnvironmentName}.json", optional: true);
                     config.AddEnvironmentVariables();
                 })
                .ConfigureLogging((context, builder) =>
                {
                    var logger = new LoggerConfiguration()
                        .MinimumLevel.Debug()
                        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                        .Enrich.FromLogContext()
                        .WriteTo.Console()
                        .CreateLogger();
                    Log.Logger = logger;
                })
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
