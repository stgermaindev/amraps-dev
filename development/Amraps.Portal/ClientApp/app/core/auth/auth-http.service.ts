import {Injectable} from '@angular/core';
import {Headers, Http, Request, RequestOptions, RequestOptionsArgs, Response, XHRBackend} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import {CoreService} from '../services/core.service';

@Injectable()
export class AuthHttpService extends Http {
    constructor(backend: XHRBackend,
                defaultOptions: RequestOptions,
                private router: Router,
                private coreService: CoreService) {
        super(backend, defaultOptions);
    }

    public request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        // this.loadingBarService.start();
        if (!options) {
            options = new RequestOptions();
        }
        if (!options.headers) {
            options.headers = new Headers();
        }

        var bearerToken = this.coreService.bearerToken;
        options.headers.set('Authorization', `Bearer ${bearerToken}`);

        return super.request(url, options)
        // .do(() => this.loadingBarService.complete())
            .catch((res: Response) => {
                // this.loadingBarService.complete();

                // if unauthorized, bounce the user back to the login screen
                if (res.status === 401) {
                    // this.authService.clearState();
                    // this.router.navigate([AuthConfig.LoginRouteUrl]);
                    console.log(`Unauthorized call`);
                }

                return Observable.throw(res);
            });
    }
}
