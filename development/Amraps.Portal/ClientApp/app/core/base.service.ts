import {Observable} from 'rxjs/Observable';
import {SwaggerException} from './swagger.exception';

export abstract class ServiceBase {
    protected baseUrl: string;

    constructor(private _baseUrl: string) {
        this.baseUrl = this._baseUrl;
    }

    public throwException(message: string, status: number, response: string,
                          headers: { [key: string]: any; }, result?: any): Observable<any> {
        if (result !== null && result !== undefined) {
            return Observable.throw(result);
        } else {
            return Observable.throw(new SwaggerException(message, status, response, headers, null));
        }
    }

    public blobToText(blob: any): Observable<string> {
        return new Observable<string>((observer: any) => {
            const reader = new FileReader();
            reader.onload = function () {
                observer.next(this.result);
                observer.complete();
            }
            reader.readAsText(blob);
        });
    }
}

