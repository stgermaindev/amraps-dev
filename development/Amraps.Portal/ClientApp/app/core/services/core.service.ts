import {Injectable, OnInit} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {environment} from '../../../enviroments/enviroment'
import {CoreConfig} from '../core.config';

declare const amrapsSettings: any;

@Injectable()
export class CoreService implements OnInit {
    private coreConfig: CoreConfig;
    private $authorizedSource = new Subject();
    public onAuthorized = this.$authorizedSource.asObservable();

    constructor() {
        console.log(`Is Production: ${environment.production}`);
        this.coreConfig = environment.appConfig as CoreConfig;
    }

    public ngOnInit(): void {
        this._token = amrapsSettings.accessToken;
    }

    private _token: string = '';
    public get bearerToken(): string {
        if(this._token.length > 0)
            return this._token;

        this._token = amrapsSettings.accessToken;
        return this._token;
    }

    public get baseApiUrl(): string {
        return this.coreConfig.baseUrl;
    }

    public get apiUrl():string {
        return this.coreConfig.apiBaseUrl;
    }

    public get authorityUrl():string {
        return this.coreConfig.authenticationAuthority;
    }    

    public authorized() {
        this.$authorizedSource.next();
    }
}
