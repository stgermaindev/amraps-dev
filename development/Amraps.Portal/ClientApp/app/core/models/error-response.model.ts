export class ErrorResponse implements IErrorResponse {
    message?: string | null;
    resultCode: number;
    referenceNumber?: string | null;
    errors?: Error[] | null;

    static fromJS(data: any): ErrorResponse {
        const result = new ErrorResponse();
        result.init(data);
        return result;
    }

    constructor(data?: IErrorResponse) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.message = data['message'] !== undefined ? data['message'] : <any>null;
            this.resultCode = data['resultCode'] !== undefined ? data['resultCode'] : <any>null;
            this.referenceNumber = data['referenceNumber'] !== undefined ? data['referenceNumber'] : <any>null;
            if (data['errors'] && data['errors'].constructor === Array) {
                this.errors = [];
                for (const item of data['errors']) {
                    this.errors.push(Error.fromJS(item));
                }
            }
        }
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['message'] = this.message !== undefined ? this.message : <any>null;
        data['resultCode'] = this.resultCode !== undefined ? this.resultCode : <any>null;
        data['referenceNumber'] = this.referenceNumber !== undefined ? this.referenceNumber : <any>null;
        if (this.errors && this.errors.constructor === Array) {
            data['errors'] = [];
            for (const item of this.errors) {
                data['errors'].push(item.toJSON());
            }
        }
        return data;
    }
}

export interface IErrorResponse {
    message?: string | null;
    resultCode: number;
    referenceNumber?: string | null;
    errors?: Error[] | null;
}

export class Error implements IError {
    message?: string | null;
    reason?: string | null;

    static fromJS(data: any): Error {
        const result = new Error();
        result.init(data);
        return result;
    }

    constructor(data?: IError) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.message = data['message'] !== undefined ? data['message'] : <any>null;
            this.reason = data['reason'] !== undefined ? data['reason'] : <any>null;
        }
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['message'] = this.message !== undefined ? this.message : <any>null;
        data['reason'] = this.reason !== undefined ? this.reason : <any>null;
        return data;
    }
}

export interface IError {
    message?: string | null;
    reason?: string | null;
}

