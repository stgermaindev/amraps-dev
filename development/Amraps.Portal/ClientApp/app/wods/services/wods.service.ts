import {Injectable} from '@angular/core';
import {ServiceBase} from '../../core/base.service';
import {AuthHttpService} from '../../core/auth/auth-http.service';
import {CoreService} from '../../core/services/core.service';
import {Observable} from 'rxjs/Observable';
import {Wod} from '../models/wod.model';
import {ErrorResponse} from '../../core/models/error-response.model';
import {Headers, Response} from '@angular/http';

@Injectable()
export class WodsService extends ServiceBase {
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(private http: AuthHttpService, private coreService: CoreService) {
        super(coreService.baseApiUrl);
    }

    get(id: number): Observable<Wod | null> {
        let url_ = this.baseUrl + '/api/v1/wods/{id}';
        if (id === undefined || id === null)
            throw new Error('The parameter \'id\' must be defined.');
        url_ = url_.replace("{id}", encodeURIComponent("" + id));
        url_ = url_.replace(/[?&]$/, "");

        let options_ = {
            method: "get",
            headers: new Headers({
                "Content-Type": "application/json",
                "Accept": "application/json"
            })
        };

        return this.http.request(url_, options_).flatMap((response_) => {
            return this.processGet(response_);
        }).catch((response_: any) => {
            if (response_ instanceof Response) {
                try {
                    return this.processGet(response_);
                } catch (e) {
                    return <Observable<Wod | null>><any>Observable.throw(e);
                }
            } else
                return <Observable<Wod | null>><any>Observable.throw(response_);
        });
    }

    protected processGet(response: Response): Observable<Wod | null> {
        const status = response.status;

        let _headers: any = response.headers ? response.headers.toJSON() : {};
        if (status === 200) {
            const _responseText = response.text();
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = resultData200 ? Wod.fromJS(resultData200) : <any>null;
            return Observable.of(result200);
        } else if (status === 400) {
            const _responseText = response.text();
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result400 = resultData400 ? ErrorResponse.fromJS(resultData400) : <any>null;
            return this.throwException("A server error occurred.", status, _responseText, _headers, result400);
        } else if (status === 500) {
            const _responseText = response.text();
            let result500: any = null;
            let resultData500 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result500 = resultData500 ? ErrorResponse.fromJS(resultData500) : <any>null;
            return this.throwException("A server error occurred.", status, _responseText, _headers, result500);
        } else if (status === 403) {
            const _responseText = response.text();
            return this.throwException("A server error occurred.", status, _responseText, _headers);
        } else if (status !== 200 && status !== 204) {
            const _responseText = response.text();
            return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
        }
        return Observable.of<Wod | null>(<any>null);
    }

    getTodaysWod(): Observable<Wod | null> {
        let url_ = this.baseUrl + "/api/v1/wods/today";
        url_ = url_.replace(/[?&]$/, "");

        let options_ = {
            method: "get",
            headers: new Headers({
                "Content-Type": "application/json",
                "Accept": "application/json"
            })
        };

        return this.http.request(url_, options_).flatMap((response_) => {
            return this.processGetTodaysWod(response_);
        }).catch((response_: any) => {
            if (response_ instanceof Response) {
                try {
                    return this.processGetTodaysWod(response_);
                } catch (e) {
                    return <Observable<Wod | null>><any>Observable.throw(e);
                }
            } else
                return <Observable<Wod | null>><any>Observable.throw(response_);
        });
    }

    protected processGetTodaysWod(response: Response): Observable<Wod | null> {
        const status = response.status;

        let _headers: any = response.headers ? response.headers.toJSON() : {};
        if (status === 200) {
            const _responseText = response.text();
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = resultData200 ? Wod.fromJS(resultData200) : <any>null;
            return Observable.of(result200);
        } else if (status === 400) {
            const _responseText = response.text();
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result400 = resultData400 ? ErrorResponse.fromJS(resultData400) : <any>null;
            return this.throwException("A server error occurred.", status, _responseText, _headers, result400);
        } else if (status === 500) {
            const _responseText = response.text();
            let result500: any = null;
            let resultData500 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result500 = resultData500 ? ErrorResponse.fromJS(resultData500) : <any>null;
            return this.throwException("A server error occurred.", status, _responseText, _headers, result500);
        } else if (status === 403) {
            const _responseText = response.text();
            return this.throwException("A server error occurred.", status, _responseText, _headers);
        } else if (status !== 200 && status !== 204) {
            const _responseText = response.text();
            return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
        }
        return Observable.of<Wod | null>(<any>null);
    }
}