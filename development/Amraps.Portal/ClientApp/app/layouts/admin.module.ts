import {NgModule} from '@angular/core';
import {PageNotFoundComponent} from '../pages/page-not-found.component';
import {AdminLayoutComponent} from './admin.layout.component';
import {NavBarComponent} from '../shared/navbar/nav-bar.component';
import {SideBarComponent} from '../shared/sidebar/side-bar.component';
import {FooterComponent} from '../shared/footer/footer.component';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from "@angular/http";
import {RouterModule} from '@angular/router';
import {CoreModule} from '../core/core.module';
import {AffiliatesModule} from '../affiliates/affiliates.module';

@NgModule({
    declarations: [
        PageNotFoundComponent,
        AdminLayoutComponent,
        NavBarComponent,
        SideBarComponent,
        FooterComponent
    ],
    imports: [
        HttpModule,
        AffiliatesModule,
        BrowserModule,
        CommonModule,
        CoreModule,
        RouterModule
    ],
    exports: [
        PageNotFoundComponent,
        AdminLayoutComponent,
        NavBarComponent,
        SideBarComponent,
        FooterComponent
    ]
})
export class AdminModule{

}