import {Component, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['../../assests/material-dashboard.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
    
}
