import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Affiliate} from '../models/affiliate.model';
import {ServiceBase} from '../../core/base.service';
import {AuthHttpService} from '../../core/auth/auth-http.service';
import {CoreService} from '../../core/services/core.service';
import {Headers, Response} from '@angular/http';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/of';
import {ErrorResponse} from "../../core/models/error-response.model";

@Injectable()
export class AffiliatesService extends ServiceBase {
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(private http: AuthHttpService, private coreService: CoreService) {
        super(coreService.baseApiUrl);
    }

    getAll(): Observable<Affiliate[] | null> {
        let url_ = this.baseUrl + "/api/v1/affiliates";
        url_ = url_.replace(/[?&]$/, "");

        let options_ = {
            method: "get",
            headers: new Headers({
                "Content-Type": "application/json",
                "Accept": "application/json"
            })
        };

        return this.http.request(url_, options_).flatMap((response_) => {
            return this.processGetAll(response_);
        }).catch((response_: any) => {
            if (response_ instanceof Response) {
                try {
                    return this.processGetAll(response_);
                } catch (e) {
                    return <Observable<Affiliate[] | null>><any>Observable.throw(e);
                }
            } else
                return <Observable<Affiliate[] | null>><any>Observable.throw(response_);
        });
    }

    protected processGetAll(response: Response): Observable<Affiliate[] | null> {
        const status = response.status;

        let _headers: any = response.headers ? response.headers.toJSON() : {};
        if (status === 200) {
            const _responseText = response.text();
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData200 && resultData200.constructor === Array) {
                result200 = [];
                for (let item of resultData200)
                    result200.push(Affiliate.fromJS(item));
            }
            return Observable.of(result200);
        } else if (status === 400) {
            const _responseText = response.text();
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result400 = resultData400 ? ErrorResponse.fromJS(resultData400) : <any>null;
            return this.throwException("A server error occurred.", status, _responseText, _headers, result400);
        } else if (status === 500) {
            const _responseText = response.text();
            let result500: any = null;
            let resultData500 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result500 = resultData500 ? ErrorResponse.fromJS(resultData500) : <any>null;
            return this.throwException("A server error occurred.", status, _responseText, _headers, result500);
        } else if (status === 403) {
            const _responseText = response.text();
            return this.throwException("A server error occurred.", status, _responseText, _headers);
        } else if (status !== 200 && status !== 204) {
            const _responseText = response.text();
            return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
        }
        return Observable.of<Affiliate[] | null>(<any>null);
    }

    get(id: number): Observable<Affiliate | null> {
        let url_ = this.baseUrl + "/api/v1/affiliates/{id}";
        if (id === undefined || id === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{id}", encodeURIComponent("" + id));
        url_ = url_.replace(/[?&]$/, "");

        let options_ = {
            method: "get",
            headers: new Headers({
                "Content-Type": "application/json",
                "Accept": "application/json"
            })
        };

        return this.http.request(url_, options_).flatMap((response_) => {
            return this.processGet(response_);
        }).catch((response_: any) => {
            if (response_ instanceof Response) {
                try {
                    return this.processGet(response_);
                } catch (e) {
                    return <Observable<Affiliate | null>><any>Observable.throw(e);
                }
            } else
                return <Observable<Affiliate | null>><any>Observable.throw(response_);
        });
    }

    protected processGet(response: Response): Observable<Affiliate | null> {
        const status = response.status;

        let _headers: any = response.headers ? response.headers.toJSON() : {};
        if (status === 200) {
            const _responseText = response.text();
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = resultData200 ? Affiliate.fromJS(resultData200) : <any>null;
            return Observable.of(result200);
        } else if (status === 400) {
            const _responseText = response.text();
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result400 = resultData400 ? ErrorResponse.fromJS(resultData400) : <any>null;
            return this.throwException("A server error occurred.", status, _responseText, _headers, result400);
        } else if (status === 500) {
            const _responseText = response.text();
            let result500: any = null;
            let resultData500 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result500 = resultData500 ? ErrorResponse.fromJS(resultData500) : <any>null;
            return this.throwException("A server error occurred.", status, _responseText, _headers, result500);
        } else if (status === 403) {
            const _responseText = response.text();
            return this.throwException("A server error occurred.", status, _responseText, _headers);
        } else if (status !== 200 && status !== 204) {
            const _responseText = response.text();
            return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
        }
        return Observable.of<Affiliate | null>(<any>null);
    }
}