import * as moment from 'moment';

export class Affiliate implements IAffiliate {
    affiliateId: number;
    name?: string | null;
    phoneNumber?: string | null;
    emailAddress?: string | null;
    website?: string | null;
    address?: Address | null;
    createdDate: moment.Moment;

    constructor(data?: IAffiliate) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.affiliateId = data["affiliateId"] !== undefined ? data["affiliateId"] : <any>null;
            this.name = data["name"] !== undefined ? data["name"] : <any>null;
            this.phoneNumber = data["phoneNumber"] !== undefined ? data["phoneNumber"] : <any>null;
            this.emailAddress = data["emailAddress"] !== undefined ? data["emailAddress"] : <any>null;
            this.website = data["website"] !== undefined ? data["website"] : <any>null;
            this.address = data["address"] ? Address.fromJS(data["address"]) : <any>null;
            this.createdDate = data["createdDate"] ? moment(data["createdDate"].toString()) : <any>null;
        }
    }

    static fromJS(data: any): Affiliate {
        let result = new Affiliate();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["affiliateId"] = this.affiliateId !== undefined ? this.affiliateId : <any>null;
        data["name"] = this.name !== undefined ? this.name : <any>null;
        data["phoneNumber"] = this.phoneNumber !== undefined ? this.phoneNumber : <any>null;
        data["emailAddress"] = this.emailAddress !== undefined ? this.emailAddress : <any>null;
        data["website"] = this.website !== undefined ? this.website : <any>null;
        data["address"] = this.address ? this.address.toJSON() : <any>null;
        data["createdDate"] = this.createdDate ? this.createdDate.toISOString() : <any>null;
        return data;
    }
}

export interface IAffiliate {
    affiliateId: number;
    name?: string | null;
    phoneNumber?: string | null;
    emailAddress?: string | null;
    website?: string | null;
    address?: Address | null;
    createdDate: moment.Moment;
}

export class Address implements IAddress {
    placeId?: string | null;
    streetAddress?: string | null;
    streetAddress2?: string | null;
    city?: string | null;
    state?: string | null;
    postalCode?: string | null;

    constructor(data?: IAddress) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.placeId = data["placeId"] !== undefined ? data["placeId"] : <any>null;
            this.streetAddress = data["streetAddress"] !== undefined ? data["streetAddress"] : <any>null;
            this.streetAddress2 = data["streetAddress2"] !== undefined ? data["streetAddress2"] : <any>null;
            this.city = data["city"] !== undefined ? data["city"] : <any>null;
            this.state = data["state"] !== undefined ? data["state"] : <any>null;
            this.postalCode = data["postalCode"] !== undefined ? data["postalCode"] : <any>null;
        }
    }

    static fromJS(data: any): Address {
        let result = new Address();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["placeId"] = this.placeId !== undefined ? this.placeId : <any>null;
        data["streetAddress"] = this.streetAddress !== undefined ? this.streetAddress : <any>null;
        data["streetAddress2"] = this.streetAddress2 !== undefined ? this.streetAddress2 : <any>null;
        data["city"] = this.city !== undefined ? this.city : <any>null;
        data["state"] = this.state !== undefined ? this.state : <any>null;
        data["postalCode"] = this.postalCode !== undefined ? this.postalCode : <any>null;
        return data;
    }
}

export interface IAddress {
    placeId?: string | null;
    streetAddress?: string | null;
    streetAddress2?: string | null;
    city?: string | null;
    state?: string | null;
    postalCode?: string | null;
}