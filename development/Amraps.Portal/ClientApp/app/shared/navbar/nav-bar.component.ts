import {Component, ElementRef, Renderer2} from '@angular/core';
import {Location} from '@angular/common';

@Component({
    selector: 'app-navbar',
    templateUrl: './nav-bar.component.html'
})
export class NavBarComponent {
    private listTitles: any[] = [];
    private nativeElement: Node;
    private toggleButton: any;
    private sidebarVisible: boolean;

    constructor(private location: Location,
                private renderer: Renderer2,
                private element: ElementRef,) {

    }

    public signOut() {
        window.location.href = 'http://localhost:4000/auth/logout';
    }

    public isMobileMenu() {
        if (window.innerWidth < 991) {
            return false;
        }
        return true;
    };

    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        setTimeout(function(){
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };

    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    };

    getTitle() {
        const title: any = this.location.prepareExternalUrl(this.location.path());
        for (let i = 0; i < this.listTitles.length; i++) {
            if (this.listTitles[i].type === 'link' && this.listTitles[i].path === title) {
                return this.listTitles[i].title;
            } else if (this.listTitles[i].type === 'sub') {
                for (let j = 0; j < this.listTitles[i].children.length; j++) {
                    const subtitle = this.listTitles[i].path + '/' + this.listTitles[i].children[j].path;
                    if (subtitle === title) {
                        return this.listTitles[i].children[j].title;
                    }
                }
            }
        }
        return '';
    }
}