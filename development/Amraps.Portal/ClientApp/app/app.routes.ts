import {Routes} from '@angular/router';
import {AdminLayoutComponent} from './layouts/admin.layout.component';
import {PageNotFoundComponent} from './pages/page-not-found.component';
import {ListAffiliatesComponent} from './affiliates/list/list-affiliates.component';
import {AffiliateResolver, AffiliatesResolver} from './affiliates/resolvers/affiliates.resolvers';
import {EditAffiliateComponent} from './affiliates/edit/edit-affiliate.component';
import {TodaysWodComponent} from './wods/components/todays-wod.component';
import {TodaysWodResolver} from './wods/resolvers/wods.resolvers';

export const appRoutes: Routes = [
    {path: '', redirectTo: 'wods/todays-wod', pathMatch: 'full'},
    {
        path: '',
        component: AdminLayoutComponent,
        children: [
            {
                path: 'wods/todays-wod',
                component: TodaysWodComponent,
                resolve: {
                    wod: TodaysWodResolver
                }
            },
            {
                path: 'affiliates/list',
                component: ListAffiliatesComponent,
                resolve: {
                    affiliates: AffiliatesResolver
                }
            }, {
                path: 'affiliates/edit/:id',
                component: EditAffiliateComponent,
                resolve: {
                    affiliate: AffiliateResolver
                }
            }
        ]
    },
    { path: '**', component: PageNotFoundComponent }
];

export const appRoutingProviders: any[] = [];