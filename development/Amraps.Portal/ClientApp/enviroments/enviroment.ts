export const environment = {
    production: false,
    appConfig: {
        authenticationAuthority: 'http://localhost:4000',
        baseUrl: 'http://localhost:4001',
        apiBaseUrl: 'http://localhost:4001/api'
    }
};
