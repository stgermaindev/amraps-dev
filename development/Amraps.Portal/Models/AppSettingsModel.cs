﻿namespace Amraps.Portal.Models
{
    public class AppSettingsModel
    {
        public string ApiUrl { get; set; }
        public string AuthUrl { get; set; }
        public string WebUrl { get; set; }
        public object AccessToken { get; set; }
        public string UserName { get; set; }
        public string UserDisplayName { get; set; }
        public string UserId { get; set; }
    }
}